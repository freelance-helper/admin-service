FROM node:10-alpine

WORKDIR /opt/admin-service
COPY package.json yarn.lock ./
RUN yarn

COPY . .
RUN yarn build

EXPOSE ${PORT}

CMD ["yarn", "start:prod"]
