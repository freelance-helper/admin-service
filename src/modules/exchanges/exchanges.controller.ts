import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
} from '@nestjs/common';
import { ExchangesService } from './exchanges.service';
import { ExchangeEntity } from './exchange.entity';
import { CreateExchangeDTO } from './exchange';

@Controller('exchanges')
export class ExchangesController {
  constructor(private readonly exchangesService: ExchangesService) {}

  @Get()
  getExchanges(): Promise<ExchangeEntity[]> {
    return this.exchangesService.getAll();
  }

  @Post()
  createExchange(@Body() body: CreateExchangeDTO): Promise<ExchangeEntity> {
    return this.exchangesService.create(body);
  }

  @Put()
  updateExchange(@Body() exchange: ExchangeEntity) {
    return this.exchangesService.update(exchange);
  }

  @Delete(':id')
  deleteExchange(@Param('id') id: number) {
    return this.exchangesService.delete(+id);
  }
}
