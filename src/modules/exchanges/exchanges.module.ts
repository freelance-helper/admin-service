import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ExchangesController } from './exchanges.controller';
import { ExchangesService } from './exchanges.service';
import { ExchangeEntity } from './exchange.entity';

@Module({
  imports: [TypeOrmModule.forFeature([ExchangeEntity])],
  controllers: [ExchangesController],
  providers: [ExchangesService],
})
export class ExchangesModule {}
