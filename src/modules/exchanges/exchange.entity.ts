import {
  Entity,
  Column,
  OneToMany,
  JoinColumn,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { OrdersConfigEntity } from '@/modules/orders-configs/orders-config.entity';

@Entity({ name: 'exchanges' })
export class ExchangeEntity {
  @PrimaryGeneratedColumn()
  exchangeId: number;

  @Column('text')
  name: string;

  @Column('text')
  url: string;

  @Column('text')
  selector: string;

  @OneToMany(() => OrdersConfigEntity, orderConfig => orderConfig.exchange)
  @JoinColumn()
  ordersConfigs: OrdersConfigEntity[];
}
