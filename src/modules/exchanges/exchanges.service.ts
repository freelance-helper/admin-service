import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateExchangeDTO } from './exchange';

import { ExchangeEntity } from './exchange.entity';

@Injectable()
export class ExchangesService {
  constructor(
    @InjectRepository(ExchangeEntity)
    private readonly exchangesRepository: Repository<ExchangeEntity>,
  ) {}

  getAll(): Promise<ExchangeEntity[]> {
    return this.exchangesRepository.find();
  }

  getOne(id: number): Promise<ExchangeEntity> {
    return this.exchangesRepository.findOne(id);
  }

  create(exchange: CreateExchangeDTO): Promise<ExchangeEntity> {
    return this.exchangesRepository.save(exchange);
  }

  update(exchange: ExchangeEntity) {
    return this.exchangesRepository.save(exchange);
  }

  async delete(id: number) {
    const exchange = await this.exchangesRepository.findOne(id);

    return this.exchangesRepository.remove(exchange);
  }
}
