export interface CreateExchangeDTO {
  name: string;
  url: string;
  selector: string;
}
