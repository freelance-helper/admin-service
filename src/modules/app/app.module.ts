import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import * as path from 'path';

import { ExchangesModule } from '@/modules/exchanges';
import { CategoriesModule } from '@/modules/categories';
import { UsersModule } from '@/modules/users';
import { ParserModule } from '@/modules/parser';
import { AuthenticateModule } from '@/modules/authenticate';
import { OrdersConfigsModule } from '@/modules/orders-configs';
import { OrdersModule } from '@/modules/orders';
import { NotificationsModule } from '@/modules/notifications';
import { OrdersTemplatesModule } from '@/modules/orders-templates';

const configsPath = path.resolve(
  __dirname,
  '../../config/**/!(*.d).config.{js,ts}',
);
const emailTemplatesPath = path.resolve(__dirname, '../../templates/email');

@Module({
  imports: [
    ConfigModule.load(configsPath, {
      modifyConfigName: name => name.replace('.config', ''),
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get('database'),
      inject: [ConfigService],
    }),
    MailerModule.forRootAsync({
      useFactory: (config: ConfigService) => ({
        transport: {
          service: 'gmail',
          auth: {
            type: 'OAuth2',
            user: config.get('mailer.email'),
            clientId: config.get('mailer.clientId'),
            clientSecret: config.get('mailer.clientSecret'),
            refreshToken: config.get('mailer.refreshToken'),
            accessToken: config.get('mailer.accessToken'),
          },
        },
        defaults: {
          from: `"Notificator" <${config.get('mailer.email')}>`,
        },
        template: {
          dir: emailTemplatesPath,
          adapter: new HandlebarsAdapter(),
          options: {
            strict: true,
          },
        },
      }),
      inject: [ConfigService],
    }),
    ExchangesModule,
    CategoriesModule,
    OrdersConfigsModule,
    UsersModule,
    AuthenticateModule,
    ParserModule,
    OrdersModule,
    NotificationsModule,
    OrdersTemplatesModule,
  ],
})
export class AppModule {}
