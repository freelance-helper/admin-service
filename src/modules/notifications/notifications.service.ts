import { HttpService, Injectable } from '@nestjs/common';
import { ConfigService } from 'nestjs-config';

import { UserEntity, UsersService } from '@/modules/users';
import { OrdersTemplatesService } from '@/modules/orders-templates';
import { TemplateTypes } from '@/modules/orders-templates/template-types';
import { CategoriesService, CategoryEntity } from '@/modules/categories';
import { LoggerService } from '@/libs/logger';
import { FirebaseMessagesService } from '@/libs/firebase-messages/firebase-messages.service';
import { IOrderInput } from '@/modules/orders';

@Injectable()
export class NotificationsService {
  constructor(
    private readonly usersService: UsersService,
    private readonly http: HttpService,
    private readonly config: ConfigService,
    private readonly ordersTemplatesService: OrdersTemplatesService,
    private readonly firebaseMessagesService: FirebaseMessagesService,
    private readonly categoriesService: CategoriesService,
    private readonly logger: LoggerService,
  ) {
  }

  async sendNotifications(order: IOrderInput) {
    const users = await this.usersService.getUsersByCategory(order.category);

    return users.forEach(user => {
      if (user.messageToken) {
        return this.sendWebPush(user, order);
      }

      if (user.tid) {
        return this.sendTelegram(user, order);
      }
    });
  }

  async sendWebPush(user: UserEntity, order: IOrderInput) {
    const messageToken = user.messageToken;
    const body = await this.getWebPushFormatMessage(order);

    this.logger.log(`Send web push to ${user.id}`, NotificationsService.name);

    return this.firebaseMessagesService.send(body, messageToken);
  }

  async sendTelegram(user: UserEntity, order: IOrderInput) {
    const url = this.getTelegramBotUrl(user.tid);
    const message = this.getTelegramFormatMessage(order);

    this.logger.log(`Send telegram to ${user.id}`, NotificationsService.name);

    return this.http
      .post(url, {
        message,
      })
      .toPromise();
  }

  getTelegramBotUrl(tid: number) {
    return `${this.config.get('app.botServiceHost')}/message/${tid}`;
  }

  formatDataMessage(template, data) {
    const d = data.reduce(
      (acc, item) => ({ ...acc, [item.config]: item.data }),
      {},
    );

    return Object.keys(d).reduce(
      (acc, key) => acc.replace(`{${key}}`, d[key]),
      template,
    );
  }

  async getWebPushFormatMessage(order: IOrderInput) {
    const category = await this.categoriesService.getById(order.category);
    const formatedTemplate = await this.getFormatMessage(order, category.exchange.exchangeId, TemplateTypes.WEB_PUSH);
    const formatBody = JSON.parse(formatedTemplate);

    return {
      ...formatBody,
      click_action: this.getOrderUrl(order, category),
    };
  }

  async getTelegramFormatMessage(order: IOrderInput) {
    const category = await this.categoriesService.getById(order.category);
    const formatByTemplate = this.getFormatMessage(order, category.exchange.exchangeId, TemplateTypes.TELEGRAM);

    const exchangeName = category.exchange.name;
    const orderLink = this.getOrderUrl(order, category);

    return `
      ${exchangeName} - ${category.name} \t
      <a href="${orderLink}">Ссылка</a>\n\n
      ${formatByTemplate}
    `;
  }

  async getFormatMessage(order: IOrderInput, exchangeId: number, templateType: TemplateTypes) {
    const template = await this.ordersTemplatesService.getTemplate(
      exchangeId,
      templateType,
    );

    return this.formatDataMessage(template.template, order.data);
  }

  getOrderUrl(order: IOrderInput, category: CategoryEntity) {
    return category.exchange.url + order.link;
  }
}
