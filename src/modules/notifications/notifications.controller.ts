import { Controller, Post, Param } from '@nestjs/common';

import { NotificationsService } from './notifications.service';
import { OrdersService } from '../orders/orders.service';

@Controller('notifications')
export class NotificationsController {
  constructor(
    private readonly notificationService: NotificationsService,
    private readonly ordersService: OrdersService,
  ) {
  }

  @Post(':id')
  async sendNotification(@Param('id') id: number) {
    const order = await this.ordersService.getById(id);

    await this.notificationService.sendNotifications({
      ...order,
      category: order.category.id,
      data: order.data.map(item => ({
        ...item,
        config: item.config.id,
      })),
    } as any);

    return;
  }
}
