import { Module, HttpModule } from '@nestjs/common';

import { NotificationsService } from './notifications.service';
import { NotificationsController } from './notifications.controller';
import { UsersModule, UsersService } from '@/modules/users';
import {
  OrdersTemplatesModule,
  OrdersTemplatesService,
} from '@/modules/orders-templates';
import { OrdersModule } from '@/modules/orders/orders.module';
import { OrdersService } from '@/modules/orders/orders.service';
import { CategoriesModule, CategoriesService } from '@/modules/categories';
import { FirebaseMessagesService } from '@/libs/firebase-messages/firebase-messages.service';

@Module({
  imports: [
    HttpModule,
    UsersModule,
    OrdersTemplatesModule,
    CategoriesModule,
    OrdersModule,
  ],
  controllers: [NotificationsController],
  providers: [
    NotificationsService,
    UsersService,
    OrdersTemplatesService,
    CategoriesService,
    FirebaseMessagesService,
    OrdersService,
  ],
  exports: [
    NotificationsService,
  ],
})
export class NotificationsModule {
}
