import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { OrdersConfigEntity } from './orders-config.entity';
import { ExchangeEntity } from '@/modules/exchanges';

interface IConfigDTO {
  selector: string;
  exchange: ExchangeEntity;
  isOrderLink: boolean;
}

interface IConfigUpdate {
  id: number;
  selector: string;
  type: string;
  isOrderLink: boolean;
}

@Injectable()
export class OrdersConfigsService {
  constructor(
    @InjectRepository(OrdersConfigEntity)
    private readonly ordersConfigsRepository: Repository<OrdersConfigEntity>,
  ) {}

  async createEntity(config: IConfigDTO): Promise<OrdersConfigEntity> {
    return this.ordersConfigsRepository.save(config);
  }

  async getAllEntities(): Promise<OrdersConfigEntity[]> {
    return this.ordersConfigsRepository.find();
  }

  async update(orderConfig: IConfigUpdate) {
    const oldOrderConfig = await this.ordersConfigsRepository.findOne(
      orderConfig.id,
    );

    return this.ordersConfigsRepository.save({
      ...oldOrderConfig,
      ...orderConfig,
    });
  }

  async getByExchange(exchange: number) {
    return this.ordersConfigsRepository.find({ where: { exchange } });
  }

  async delete(id: number) {
    const orderConfig = await this.ordersConfigsRepository.findOne(id);

    return this.ordersConfigsRepository.remove(orderConfig);
  }
}
