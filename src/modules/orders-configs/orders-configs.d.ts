export interface IOrdersConfigsInput {
  selector: string;
  exchange: number;
  type: string;
  isOrderLink: boolean;
}
