import { ManyToOne, Column, PrimaryGeneratedColumn, Entity } from 'typeorm';
import { ExchangeEntity } from '@/modules/exchanges';

@Entity({ name: 'orders_configs' })
export class OrdersConfigEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  selector: string;

  @ManyToOne(type => ExchangeEntity, exchange => exchange.exchangeId)
  exchange: ExchangeEntity;

  @Column('text')
  type: string; // text | href

  @Column('boolean')
  isOrderLink: boolean;
}
