import {
  Controller,
  Post,
  Put,
  Get,
  Delete,
  Param,
  Query,
  Body,
} from '@nestjs/common';

import { OrdersConfigsService } from './orders-configs.service';
import { IOrdersConfigsInput } from './orders-configs';
import { OrdersConfigEntity } from './orders-config.entity';
import { ExchangesService } from '@/modules/exchanges';

interface IOrderConfigUpdate extends IOrdersConfigsInput {
  id: number;
}

@Controller('orders-configs')
export class OrdersConfigsController {
  constructor(
    private readonly ordersConfigsService: OrdersConfigsService,
    private readonly exchangesService: ExchangesService,
  ) {}

  @Post()
  async createOrdersConfig(
    @Body() data: IOrdersConfigsInput,
  ): Promise<OrdersConfigEntity> {
    const exchange = await this.exchangesService.getOne(data.exchange);

    return this.ordersConfigsService.createEntity({
      ...data,
      exchange,
    });
  }

  @Put()
  async updateOrderConfig(@Body() orderConfig: IOrderConfigUpdate) {
    const { exchange, ...updatedOrderConfig } = orderConfig;

    return this.ordersConfigsService.update(updatedOrderConfig);
  }

  @Get()
  async getAllOrdersConfigs(
    @Query('exchange') exchange: number,
  ): Promise<OrdersConfigEntity[]> {
    return exchange
      ? this.ordersConfigsService.getByExchange(exchange)
      : this.ordersConfigsService.getAllEntities();
  }

  @Delete(':id')
  async deleteOrderCategory(@Param('id') id: number) {
    return this.ordersConfigsService.delete(id);
  }
}
