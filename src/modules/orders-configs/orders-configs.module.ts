import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrdersConfigsController } from './orders-configs.controller';
import { OrdersConfigsService } from './orders-configs.service';
import { OrdersConfigEntity } from './orders-config.entity';
import { ExchangesModule, ExchangesService } from '@/modules/exchanges';

@Module({
  imports: [TypeOrmModule.forFeature([OrdersConfigEntity]), ExchangesModule],
  controllers: [OrdersConfigsController],
  providers: [OrdersConfigsService, ExchangesService],
})
export class OrdersConfigsModule {}
