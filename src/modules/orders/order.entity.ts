import {
  Entity,
  OneToMany,
  Index,
  JoinColumn,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
} from 'typeorm';

import { OrderDataEntity } from './order-data.entity';
import { CategoryEntity } from '@/modules/categories';

@Entity('orders')
export class OrderEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { length: 200 })
  @Index()
  link: string;

  @ManyToOne(type => CategoryEntity, category => category.id, {
    cascade: ['insert', 'remove'],
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  category: CategoryEntity;

  @OneToMany(type => OrderDataEntity, orderData => orderData.order, {
    cascade: ['insert', 'remove'],
    onDelete: 'CASCADE',
  })
  @JoinColumn()
  data: OrderDataEntity[];
}
