import { Controller, Post, Body, Get } from '@nestjs/common';

import { OrdersService } from './orders.service';
import { IOrderInput } from './order.d';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  createOrder(@Body('data') order: IOrderInput) {
    return this.ordersService.createOrder(order);
  }

  @Get()
  async getOrders() {
    return this.ordersService.getAll();
  }
}
