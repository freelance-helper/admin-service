import {
  Entity,
  Column,
  JoinColumn,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';

import { OrdersConfigEntity } from '../orders-configs';
import { OrderEntity } from './order.entity';

@Entity('orders_data')
export class OrderDataEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  data: string;

  @ManyToOne(type => OrdersConfigEntity, orderConfig => orderConfig.id)
  @JoinColumn()
  config: OrdersConfigEntity;

  @ManyToOne(type => OrderEntity, order => order.data, { onDelete: 'CASCADE' })
  @JoinColumn()
  order: OrderEntity;
}
