import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Injectable } from '@nestjs/common';

import { IOrderInput } from './order.d';
import { OrderEntity } from './order.entity';
import { OrderDataEntity } from './order-data.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(OrderEntity)
    private readonly orderRepository: Repository<OrderEntity>,
    @InjectRepository(OrderDataEntity)
    private readonly orderDataRepository: Repository<OrderDataEntity>,
  ) {}

  createOrder(order: IOrderInput) {
    return this.orderRepository.save(order as any);
  }

  async isExist(link: string) {
    const res = await this.orderRepository.findOne({ link });

    return !!res;
  }

  getAll() {
    return this.orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.data', 'data')
      .leftJoinAndSelect('order.category', 'category')
      .leftJoinAndSelect('data.config', 'config')
      .getMany();
  }

  getById(id: number) {
    return this.orderRepository
      .createQueryBuilder('order')
      .leftJoinAndSelect('order.data', 'data')
      .leftJoinAndSelect('data.config', 'config')
      .leftJoinAndSelect('order.category', 'category')
      .where('order.id = :id', {id})
      .getOne();
  }
}
