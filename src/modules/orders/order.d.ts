export interface IOrderInput {
  id?: number;
  link: string;
  category: number;
  data: IOrderConfigItem[];
}

export interface IOrderConfigItem {
  id?: number;
  config: number;
  data: string;
}
