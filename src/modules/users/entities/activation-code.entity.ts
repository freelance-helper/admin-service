import {
  Entity,
  OneToOne,
  Column,
  PrimaryGeneratedColumn,
  JoinColumn,
} from 'typeorm';

import { UserCredentialEntity } from './user-credential.entity';

@Entity('activation_codes')
export class ActivationCodeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => UserCredentialEntity, { onDelete: 'CASCADE' })
  @JoinColumn()
  credentials: UserCredentialEntity;

  @Column('boolean', { default: false })
  active: boolean;
}
