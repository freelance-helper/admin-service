import {
  Entity,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
  Column,
} from 'typeorm';

import { UserCredentialEntity } from './user-credential.entity';
import { UserRoles } from '../user-roles';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  email: string;

  @Column('int', { nullable: true })
  tid: number;

  @Column('text', { nullable: true })
  messageToken: string;

  @OneToOne(() => UserCredentialEntity, { onDelete: 'CASCADE' })
  @JoinColumn()
  credentials: UserCredentialEntity;

  @Column('text')
  role: UserRoles;
}
