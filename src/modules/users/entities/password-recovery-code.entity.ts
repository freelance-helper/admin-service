import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { RecoveryStatus } from '../recovery-status';

@Entity('password_recovery_codes')
export class PasswordRecoveryCodeEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('text')
  email: string;

  @Column('text')
  code: string;

  @Column('date')
  expires: string;

  @Column('date')
  createdAt: string;

  @Column('varchar', { length: 10 })
  status: RecoveryStatus;
}
