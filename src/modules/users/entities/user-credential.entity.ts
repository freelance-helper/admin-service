import {
  Entity,
  OneToOne,
  JoinColumn,
  PrimaryGeneratedColumn,
  Column,
} from 'typeorm';

import { UserEntity } from './user.entity';
import { ActivationCodeEntity } from '@/modules/users/entities/activation-code.entity';

@Entity({ name: 'users_credentials' })
export class UserCredentialEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => ActivationCodeEntity, { onDelete: 'CASCADE' })
  @JoinColumn()
  activationCode: ActivationCodeEntity;

  @Column('text')
  password: string;

  @Column('text')
  salt: string;

  @OneToOne(() => UserEntity, user => user.credentials, { onDelete: 'CASCADE' })
  @JoinColumn()
  user: UserEntity;
}
