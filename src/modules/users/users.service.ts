import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as moment from 'moment';
import * as bcrypt from 'bcrypt';

import { UserRoles } from './user-roles';
import { UserEntity } from './entities/user.entity';
import { UserCredentialEntity } from './entities/user-credential.entity';
import { UsersCategoriesEntity } from '@/modules/categories/entities/users-categories.entity';
import { UserCategoryStatuses } from '../categories/user-category-statuses';
import { PasswordRecoveryCodeEntity } from './entities/password-recovery-code.entity';
import { RecoveryStatus } from '@/modules/users/recovery-status';
import { ActivationCodeEntity } from './entities/activation-code.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    @InjectRepository(UserCredentialEntity)
    private readonly credentialsRepository: Repository<UserCredentialEntity>,
    @InjectRepository(PasswordRecoveryCodeEntity)
    private readonly passwordRecoveryCodeRepository: Repository<
      PasswordRecoveryCodeEntity
    >,
    @InjectRepository(ActivationCodeEntity)
    private readonly activationCodeRepository: Repository<ActivationCodeEntity>,
  ) {}

  getUsersByCategory(categoryId) {
    return this.usersRepository
      .createQueryBuilder('user')
      .where(qb => {
        const subQuery = qb
          .subQuery()
          .select('userCategory.user')
          .from(UsersCategoriesEntity, 'userCategory')
          .where('userCategory.category = :categoryId')
          .andWhere('userCategory.status = :status')
          .getQuery();

        return 'user.id IN ' + subQuery;
      })
      .setParameter('status', UserCategoryStatuses.ACTIVE)
      .setParameter('categoryId', categoryId)
      .getMany();
  }

  async createRecoveryCode(email: string) {
    const code = this.getActivationCode();
    const expires = this.getCodeExpireDate();
    const status = RecoveryStatus.PENDING;
    const createdAt = this.getNow();

    return this.passwordRecoveryCodeRepository.save({
      code,
      expires,
      status,
      email,
      createdAt,
    });
  }

  async getCodesByCurrentDay(email: string) {
    const now = this.getNow();

    return this.passwordRecoveryCodeRepository.find({
      where: {
        createdAt: now,
        email,
      },
    });
  }

  async getCodesByEmail(email: string) {
    return this.passwordRecoveryCodeRepository.find({ where: { email } });
  }

  async setCanceledByEmail(email: string) {
    return this.passwordRecoveryCodeRepository
      .createQueryBuilder()
      .update()
      .set({ status: RecoveryStatus.CANCELED })
      .where('email = :email', { email })
      .andWhere('status = :activeStatus', {
        activeStatus: RecoveryStatus.PENDING,
      })
      .execute();
  }

  async updateExpiredCodes(email: string) {
    return this.passwordRecoveryCodeRepository
      .createQueryBuilder()
      .update()
      .set({ status: RecoveryStatus.EXPIRED })
      .where('email = :email', { email })
      .andWhere('expires < :now', { now: this.getNow() })
      .execute();
  }

  async createUser(
    email: string,
    password: string,
    role: UserRoles = UserRoles.USER,
  ): Promise<UserEntity> {
    const user = await this.usersRepository.save({ email, role });

    const hashed = await this.getHashedPassword(password);

    const credentials = await this.credentialsRepository.save({
      password: hashed.password,
      salt: hashed.salt,
      user,
    });
    credentials.activationCode = await this.activationCodeRepository.save({
      credentials,
    });
    await this.credentialsRepository.save(credentials);

    return this.usersRepository.save({ ...user, credentials });
  }

  async deleteUser(userId: number) {
    const user = await this.usersRepository.findOne(userId);

    return this.usersRepository.remove(user);
  }

  async passwordIsValid(userId: number, password: string) {
    const credentials = await this.getUserCredentials(userId);

    return bcrypt.compare(password, credentials.password);
  }

  async getRestoreCodeById(id: string) {
    return this.passwordRecoveryCodeRepository.findOne(id);
  }

  async updateUserCredentials(userId: number, password: string) {
    const credentials = await this.getUserCredentials(userId);

    const hashed = await this.getHashedPassword(password);
    credentials.password = hashed.password;
    credentials.salt = hashed.salt;

    return this.credentialsRepository.save(credentials);
  }

  async getAll(): Promise<UserEntity[]> {
    return this.usersRepository.find();
  }

  async getUserCredentials(userId: number): Promise<UserCredentialEntity> {
    return this.credentialsRepository.findOne({
      where: { userId },
      relations: ['activationCode'],
    });
  }

  async getByEmail(email: string): Promise<UserEntity> {
    return this.usersRepository.findOne({ where: { email } });
  }

  async getById(id: number): Promise<UserEntity> {
    return this.usersRepository.findOne(id);
  }

  async addTelegramAccount(userId: number, tid: number) {
    const user = await this.getById(userId);

    user.tid = tid;

    return this.usersRepository.save(user);
  }

  async sendMessageToken(messageToken: string, userId: number) {
    const user = await this.usersRepository.findOne(userId);

    user.messageToken = messageToken;

    return this.usersRepository.save(user);
  }

  async findTelegramAccount(tid: number) {
    return this.usersRepository.findOne({ where: { tid } });
  }

  private async getHashedPassword(password: string) {
    const saltRounds = 10;
    const salt = await bcrypt.genSalt(saltRounds);
    const hashedPassword = await bcrypt.hash(password, salt);

    return {
      salt,
      password: hashedPassword,
    };
  }

  private getNow() {
    return moment()
      .toISOString()
      .split('T')[0];
  }

  private getCodeExpireDate() {
    return moment()
      .add('3', 'day')
      .toISOString()
      .split('T')[0];
  }

  private getActivationCode() {
    return String(Math.floor(Math.random() * 1000000)).padStart(6, '0');
  }
}
