import {
  Controller,
  UseGuards,
  Get,
  Body,
  Param,
  Res,
  Put,
  Post,
} from '@nestjs/common';

import { UsersService } from '../users.service';
import { UserEntity } from '../entities/user.entity';
import { AuthenticateGuard } from '../../authenticate/authenticate.guard';
import { UserID } from '../../authenticate/user-id.decorator';
import { FirebaseMessagesService } from '@/libs/firebase-messages/firebase-messages.service';

@Controller('users')
@UseGuards(AuthenticateGuard)
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly firebaseMessagesService: FirebaseMessagesService,
  ) {}

  @Get()
  async getAllUsers(): Promise<UserEntity[]> {
    return this.usersService.getAll();
  }

  @Get(':tid/telegram')
  async getTelegramAccount(@Param('tid') tid: number, @Res() res) {
    const user = await this.usersService.findTelegramAccount(tid);

    if (user) {
      return res.send({
        userId: user.id,
      });
    } else {
      return res.status(404).send({ message: 'Not found' });
    }
  }

  @Put(':userId/message-token')
  async setMessageToken(
    @Body('messageToken') messageToken: string,
    @UserID() userId: number,
  ) {
    await this.usersService.sendMessageToken(messageToken, userId);

    return;
  }

  @Post(':userId/message')
  async sendUserMessage(@UserID() userId: number) {
    const user = await this.usersService.getById(userId);

    const body = {
      title: 'Подаруй',
      body: 'Сьогодні відбувся розподіл участників у події',
    };

    return this.firebaseMessagesService.send(body, user.messageToken);
  }
}
