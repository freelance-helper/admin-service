import {
  Controller,
  UseGuards,
  Post,
  Get,
  Res,
  Put,
  Body,
} from '@nestjs/common';
import { ConfigService } from 'nestjs-config';
import { MailerService } from '@nest-modules/mailer';

import { LoggerService } from '@/libs/logger';
import { AuthenticateGuard } from '@/modules/authenticate/authenticate.guard';
import { UsersService } from '../users.service';

@Controller('users')
@UseGuards(AuthenticateGuard)
export class UsersAuthenticateController {
  constructor(
    private readonly mailer: MailerService,
    private readonly config: ConfigService,
    private readonly logger: LoggerService,
    private readonly userService: UsersService,
  ) {}

  @Post('restore-password')
  async createRestoreToken(@Body('email') email: string, @Res() res: any) {
    const codes = await this.userService.getCodesByCurrentDay(email);

    if (codes.length > 3) {
      return res.status(429).send({ error: true, message: 'To many request' });
    }

    await Promise.all([
      this.userService.updateExpiredCodes(email),
      this.userService.setCanceledByEmail(email),
    ]);

    const code = await this.userService.createRecoveryCode(email);
    const emailPayload = {
      to: email,
      subject: 'Сбросить пароль!',
      template: 'restore-password',
      context: {
        link: `${this.config.get(
          'app.frontUrl',
        )}/authenticate/restore-password?id=${code.id}`,
        front: `${this.config.get('app.frontUrl')}/`,
      },
    };

    try {
      await this.mailer.sendMail(emailPayload);

      res
        .status(201)
        .send({ message: 'Код для восстановления пароля отправлен на почту' });
    } catch (e) {
      res
        .status(500)
        .send({ message: 'Извините, произошла ошибка, попробуйте позже' });
      this.logger.error(
        'Send email error',
        e,
        UsersAuthenticateController.name,
      );
    }
  }

  @Put('restore-password')
  async restorePassword(@Body() body: any) {
    const restoreId = body.restoreId;
    const password = body.password;

    const restoreCode = await this.userService.getRestoreCodeById(restoreId);

    if (restoreCode) {
      const user = await this.userService.getByEmail(restoreCode.email);
      await this.userService.updateUserCredentials(user.id, password);
    }

    return { message: 'Пароль успешно изменен' };
  }

  @Get('restore-password')
  getRestoreTokens() {
    return this.userService.getCodesByEmail('antsiferovmaximv@gmail.com');
  }
}

// create restore password code
// send restore password code
// activate account
