import {
  Controller,
  UseGuards,
  Get,
  Post,
  Body,
  Param,
  Delete,
} from '@nestjs/common';

import { AuthenticateGuard } from '@/modules/authenticate/authenticate.guard';
import { UserID } from '@/modules/authenticate/user-id.decorator';
import { CategoriesService } from '@/modules/categories';
import { UsersService } from '../users.service';
import { UserCategoryStatuses } from '../../categories/user-category-statuses';

@Controller('users/:userId')
@UseGuards(AuthenticateGuard)
export class UsersCategoriesController {
  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly usersService: UsersService,
  ) {}

  @Get('categories')
  getUserCategories(@Param('userId') userId: number) {
    return this.categoriesService.getUserCategories(userId);
  }

  @Post('categories/status')
  setCategoryStatus(
    @Param('userId') userId: number,
    @Body('status') status: UserCategoryStatuses,
  ) {
    return this.categoriesService.setUserCategoriesStatus(userId, status);
  }

  @Post('categories/:categoryId')
  async addUserCategory(
    @Param('categoryId') categoryId: number,
    @Param('userId') userId: number,
  ) {
    const category = await this.categoriesService.getById(categoryId);
    const user = await this.usersService.getById(userId);
    let userCategory;

    try {
      userCategory = await this.categoriesService.createUserCategory(
        user,
        category,
      );
    } catch (e) {
      return {
        error: true,
        message: 'Category already exist',
      };
    }

    return userCategory;
  }

  @Delete('categories/:categoryId')
  async deleteUserCategory(
    @Param('categoryId') categoryId: number,
    @UserID() userId: number,
  ) {
    await this.categoriesService.deleteUserCategory(userId, categoryId);

    return;
  }
}
