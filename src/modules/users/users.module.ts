import { Module, HttpModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserEntity } from './entities/user.entity';
import { ActivationCodeEntity } from './entities/activation-code.entity';
import { UserCredentialEntity } from './entities/user-credential.entity';
import { PasswordRecoveryCodeEntity } from './entities/password-recovery-code.entity';
import { UsersService } from './users.service';
import { UsersController } from './controllers/users.controller';
import { UsersCategoriesController } from './controllers/users-categories.controller';
import { UsersAuthenticateController } from './controllers/users-authenticate.controller';
import { CategoriesService } from '@/modules/categories/categories.service';
import { CategoriesModule } from '@/modules/categories/categories.module';
import { FirebaseMessagesService } from '@/libs/firebase-messages/firebase-messages.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      UserEntity,
      UserCredentialEntity,
      PasswordRecoveryCodeEntity,
      ActivationCodeEntity,
    ]),
    CategoriesModule,
    HttpModule,
  ],
  controllers: [
    UsersController,
    UsersCategoriesController,
    UsersAuthenticateController,
  ],
  providers: [UsersService, CategoriesService, FirebaseMessagesService],
  exports: [UsersService],
})
export class UsersModule {}
