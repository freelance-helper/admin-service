export enum RecoveryStatus {
  ACTIVATED = 'ACTIVATED',
  PENDING = 'PENDING',
  EXPIRED = 'EXPIRED',
  CANCELED = 'CANCELED',
}
