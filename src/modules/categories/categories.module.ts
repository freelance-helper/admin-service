import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { CategoriesController } from './categories.controller';
import { CategoriesService } from './categories.service';
import { CategoryEntity } from './entities/category.entity';
import { UsersCategoriesEntity } from './entities/users-categories.entity';
import { ExchangesModule, ExchangesService } from '../exchanges';

@Module({
  imports: [
    TypeOrmModule.forFeature([CategoryEntity, UsersCategoriesEntity]),
    ExchangesModule,
  ],
  controllers: [CategoriesController],
  providers: [CategoriesService, ExchangesService],
})
export class CategoriesModule {}
