import {
  Controller,
  Get,
  Post,
  Delete,
  Put,
  Param,
  Body,
} from '@nestjs/common';

import { CategoriesService } from './categories.service';
import { CategoryEntity } from './entities/category.entity';
import { ICategoryCreateInput, ICategoryUpdateInput } from './categories';
import { ExchangesService } from '../exchanges';
import { UserCategoryStatuses } from '@/modules/categories/user-category-statuses';

@Controller('categories')
export class CategoriesController {
  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly exchangesService: ExchangesService,
  ) {}

  @Post()
  async createCategory(
    @Body() category: ICategoryCreateInput,
  ): Promise<CategoryEntity> {
    const exchange = await this.exchangesService.getOne(category.exchangeId);
    const parent = category.parentId
      ? await this.categoriesService.getById(category.parentId)
      : undefined;

    return this.categoriesService.createCategory(
      category.name,
      parent,
      exchange,
      category.url,
    );
  }

  @Get('active')
  getActiveCategories() {
    return this.categoriesService.getActiveCategories();
  }

  @Put()
  async updateCategory(@Body() category: ICategoryUpdateInput) {
    return this.categoriesService.updateCategory(category);
  }

  @Get('/exchange/:exchangeId')
  getCategoriesTree(@Param('exchangeId') exchangeId: number) {
    return this.categoriesService.getTreeByExchange(Number(exchangeId));
  }

  @Get('tree')
  getTreeCategories() {
    return this.categoriesService.getTree();
  }

  @Get()
  getAllCategories(): Promise<CategoryEntity[]> {
    return this.categoriesService.getAll();
  }

  @Delete(':id')
  async deleteCategory(@Param('id') id: number) {
    await this.categoriesService.deleteCategory(id);
    return;
  }
}
