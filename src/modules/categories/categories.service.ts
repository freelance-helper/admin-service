import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, TreeRepository } from 'typeorm';

import { UserEntity } from '../users/entities/user.entity';
import { UsersCategoriesEntity } from './entities/users-categories.entity';
import { CategoryEntity } from './entities/category.entity';
import { ExchangeEntity } from '../exchanges';
import { UserCategoryStatuses } from './user-category-statuses';
import { ICategoryUpdateInput } from './categories';

@Injectable()
export class CategoriesService {
  constructor(
    @InjectRepository(CategoryEntity)
    private readonly categoriesRepository: TreeRepository<CategoryEntity>,
    @InjectRepository(UsersCategoriesEntity)
    private readonly usersCategoriesRepository: Repository<
      UsersCategoriesEntity
    >,
  ) {}

  async updateCategory(newCategory: ICategoryUpdateInput) {
    const category = await this.categoriesRepository.findOne(newCategory.id);

    return this.categoriesRepository.save({ ...category, ...newCategory });
  }

  async getTreeByExchange(exchangeId: number): Promise<any[]> {
    const roots = await this.categoriesRepository
      .createQueryBuilder('category')
      .where('category.exchange = :exchangeId', { exchangeId })
      .andWhere('category.parent IS NULL')
      .getMany();

    return Promise.all(
      roots.map(category =>
        this.categoriesRepository.findDescendantsTree(category),
      ),
    );
  }

  async getTree() {
    return this.categoriesRepository.findTrees();
  }

  getAll(): Promise<CategoryEntity[]> {
    return this.categoriesRepository.find();
  }

  createCategory(
    name: string,
    parent: CategoryEntity,
    exchange: ExchangeEntity,
    url: string,
  ): Promise<CategoryEntity> {
    return this.categoriesRepository.save({
      name,
      parent,
      exchange,
      url,
    });
  }

  getById(id: number): Promise<CategoryEntity> {
    return this.categoriesRepository.findOne(id, { relations: ['exchange'] });
  }

  async deleteCategory(id) {
    const category = await this.getById(id);

    return this.categoriesRepository.remove(category);
  }

  async createUserCategory(user: UserEntity, category: CategoryEntity) {
    const userCategory = await this.usersCategoriesRepository.findOne({
      where: { user: user.id },
    });

    const status = userCategory
      ? userCategory.status
      : UserCategoryStatuses.INACTIVE;

    return this.usersCategoriesRepository.save({ user, category, status });
  }

  async setUserCategoriesStatus(userId: number, status: UserCategoryStatuses) {
    const userCategories = await this.usersCategoriesRepository.find({
      where: { user: userId },
    });
    const updatedCategories = userCategories.map(category => ({
      ...category,
      status,
    }));

    return this.usersCategoriesRepository.save(updatedCategories);
  }

  getActiveCategories() {
    return this.categoriesRepository
      .createQueryBuilder('category')
      .leftJoinAndSelect('category.exchange', 'exchange')
      .leftJoinAndSelect('exchange.ordersConfigs', 'ordersConfigs')
      .andWhere(qb => {
        const subQuery = qb
          .subQuery()
          .select('users_categories.category')
          .from(UsersCategoriesEntity, 'users_categories')
          .where('users_categories.status = :status')
          .getQuery();

        return 'category.id IN ' + subQuery;
      })
      .setParameter('status', UserCategoryStatuses.ACTIVE)
      .getMany();
  }

  getCategoriesWithUsers() {
    return this.categoriesRepository
      .createQueryBuilder('category')
      .leftJoinAndSelect('category.exchange', 'exchange')
      .leftJoinAndSelect('exchange.ordersConfigs', 'ordersConfigs')
      .where(qb => {
        const subQuery = qb
          .subQuery()
          .select('users_categories.category')
          .from(UsersCategoriesEntity, 'users_categories')
          .getQuery();

        return 'category.id IN ' + subQuery;
      })
      .getMany();
  }

  getUserCategories(userId) {
    return this.categoriesRepository
      .createQueryBuilder('category')
      .where(qb => {
        const subQuery = qb
          .subQuery()
          .select('users_categories.category')
          .from(UsersCategoriesEntity, 'users_categories')
          .where('users_categories.user = :userId')
          .getQuery();

        return 'category.id IN ' + subQuery;
      })
      .leftJoinAndSelect('category.exchange', 'exchange')
      .setParameter('userId', userId)
      .getMany();
  }

  deleteUserCategory(user, category) {
    return this.usersCategoriesRepository
      .createQueryBuilder()
      .delete()
      .where('user = :user', { user })
      .andWhere('category = :category', { category })
      .execute();
  }
}
