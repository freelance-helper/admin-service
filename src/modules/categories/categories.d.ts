export interface ICategoryCreateInput {
  name: string;
  exchangeId: number;
  parentId?: number;
  url: string;
}

export interface ICategoryUpdateInput {
  id: number;
  name: string;
  url: string;
}
