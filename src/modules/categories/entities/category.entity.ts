import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  ManyToOne,
  Tree,
  TreeChildren,
  TreeParent,
} from 'typeorm';
import { ExchangeEntity } from '@/modules/exchanges';

@Entity({ name: 'categories' })
@Tree('nested-set')
export class CategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('text')
  name: string;

  @Column('text')
  url: string;

  @ManyToOne(type => ExchangeEntity, exchange => exchange.exchangeId)
  exchange: ExchangeEntity;

  @TreeParent()
  parent: CategoryEntity;

  @TreeChildren()
  children: CategoryEntity[];
}
