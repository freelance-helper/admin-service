import {
  Entity,
  JoinColumn,
  Unique,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
} from 'typeorm';

import { CategoryEntity } from '@/modules/categories/entities/category.entity';
import { UserCategoryStatuses } from '../user-category-statuses';
import { UserEntity } from '../../users/entities/user.entity';

@Entity('users_categories')
@Unique('USER_CATEGORY', ['category', 'user'])
export class UsersCategoriesEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => CategoryEntity, category => category.id)
  @JoinColumn()
  category: CategoryEntity;

  @ManyToOne(() => UserEntity, user => user.id, { onDelete: 'CASCADE' })
  user: UserEntity;

  @Column('text')
  status: UserCategoryStatuses;
}
