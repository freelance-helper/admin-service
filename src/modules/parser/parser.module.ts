import { Module, HttpModule } from '@nestjs/common';

import { ParserController } from './parser.controller';
import { CategoriesModule, CategoriesService } from '@/modules/categories';
import { OrdersModule, OrdersService } from '@/modules/orders';
import { ParseService } from './parse.service';
import { ParseScheduleService } from './parse-schedule.service';
import { NotificationsModule } from '@/modules/notifications/notifications.module';
import { NotificationsService } from '@/modules/notifications/notifications.service';
import { UsersModule, UsersService } from '@/modules/users';
import {
  OrdersTemplatesModule,
  OrdersTemplatesService,
} from '@/modules/orders-templates';
import { LoggerModule, LoggerService } from '@/libs/logger';
import { FirebaseMessagesService } from '@/libs/firebase-messages/firebase-messages.service';

@Module({
  imports: [
    CategoriesModule,
    OrdersModule,
    NotificationsModule,
    UsersModule,
    OrdersTemplatesModule,
    LoggerModule,
    HttpModule,
  ],
  controllers: [ParserController],
  providers: [
    CategoriesService,
    OrdersService,
    ParseService,
    ParseScheduleService,
    UsersService,
    OrdersTemplatesService,
    LoggerService,
    FirebaseMessagesService,
    NotificationsService,
  ],
})
export class ParserModule {}
