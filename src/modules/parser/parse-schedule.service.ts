import { Injectable } from '@nestjs/common';
import { Interval, NestSchedule } from 'nest-schedule';

import { ParseService } from './parse.service';
import { CategoriesService } from '@/modules/categories';
import { OrdersService } from '@/modules/orders';
import { NotificationsService } from '@/modules/notifications';
import { LoggerService } from '@/libs/logger';

@Injectable()
export class ParseScheduleService extends NestSchedule {
  constructor(
    private readonly parseService: ParseService,
    private readonly categoriesService: CategoriesService,
    private readonly ordersService: OrdersService,
    private readonly notificationsService: NotificationsService,
    private readonly loggerService: LoggerService,
  ) {
    super();
  }

  @Interval(50000)
  async parse() {
    const categories = await this.categoriesService.getActiveCategories();

    const orders = await this.parseService.parseCategories(categories);

    this.loggerService.log(`Parse ${categories.length} categories`, ParseScheduleService.name);

    for (const order of orders) {
      const isExist = await this.ordersService.isExist(order.link);

      if (!isExist) {
        this.loggerService.log('Order not exist', ParseScheduleService.name);

        await this.ordersService.createOrder(order);

        try {
          this.loggerService.log(
            'Send notification',
            ParseScheduleService.name,
          );
          this.notificationsService.sendNotifications(order);
        } catch (e) {
          this.loggerService.error(
            'Cannot send telegram message',
            e.stackTrace,
          );
        }
      }
    }
  }
}
