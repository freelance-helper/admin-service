import { Injectable } from '@nestjs/common';
import * as cheerio from 'cheerio';
import * as needle from 'needle';

import { IOrderInput } from '@/modules/orders/order';

@Injectable()
export class ParseService {
  async parseCategories(categories: any[]) {
    if (categories.length === 0) {
      return;
    }

    const res = await Promise.all(categories.map(this.parseCategory));
    return res.reduce((acc, item) => [...acc, ...item]);
  }

  parseCategory = async (category): Promise<IOrderInput[]> => {
    const url = this.getCategoryUrl(category);
    const orderId = category.exchange.ordersConfigs.find(
      orderConfig => orderConfig.isOrderLink,
    );
    const configs = category.exchange.ordersConfigs;

    const body = await this.fetchBody(url);
    const $ = cheerio.load(body, { decodeEntities: true });

    const orders = [];
    const rows = Array.from($(category.exchange.selector));

    for (const row of rows) {
      const data = [];
      const link = $(row)
        .find(orderId.selector)
        .attr('href');

      for (const config of configs) {
        const targetText = $(row)
          .find(config.selector)
          .text();

        data.push({
          config: config.id,
          data: targetText,
        });
      }

      orders.push({
        link,
        category: category.id,
        data,
      });
    }

    return orders;
  }

  fetchBody(url) {
    return new Promise((resolve, reject) => {
      needle.get(url, (err, res) => (err ? reject(err) : resolve(res.body)));
    });
  }

  getCategoryUrl = category => {
    const exchangeUrl = category.exchange.url;

    return exchangeUrl + category.url;
  };
}
