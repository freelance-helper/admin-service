import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { TemplateTypes } from './template-types';
import { ExchangeEntity } from '@/modules/exchanges';
import { OrdersTemplateEntity } from './orders-template.entity';

@Injectable()
export class OrdersTemplatesService {
  constructor(
    @InjectRepository(OrdersTemplateEntity)
    private readonly ordersTemplateRepository: Repository<OrdersTemplateEntity>,
  ) {}

  createTemplate(
    exchange: ExchangeEntity,
    template: string,
    type: TemplateTypes,
  ) {
    return this.ordersTemplateRepository.save({
      exchange,
      template,
      type,
    });
  }

  getTemplate(exchangeId: number, type: string) {
    return this.ordersTemplateRepository.findOne({
      where: {
        exchange: exchangeId,
        type,
      },
    });
  }
}
