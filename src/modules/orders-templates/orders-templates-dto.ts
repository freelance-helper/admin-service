import { TemplateTypes } from './template-types';

export interface CreateTemplateBody {
  exchangeId: number;
  template: string;
  type: TemplateTypes;
}
