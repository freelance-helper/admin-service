import { Entity, ManyToOne, Column, PrimaryGeneratedColumn } from 'typeorm';

import { ExchangeEntity } from '@/modules/exchanges';
import { TemplateTypes } from './template-types';

@Entity({ name: 'orders_templates' })
export class OrdersTemplateEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => ExchangeEntity, exchange => exchange.exchangeId)
  exchange: ExchangeEntity;

  @Column('text')
  template: string;

  @Column('text')
  type: TemplateTypes;
}
