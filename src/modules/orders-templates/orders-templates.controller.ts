import { Controller, Post, Body } from '@nestjs/common';

import { ExchangesService } from '@/modules/exchanges';
import { OrdersTemplatesService } from './orders-templates.service';
import { CreateTemplateBody } from './orders-templates-dto';

@Controller('orders-templates')
export class OrdersTemplatesController {
  constructor(
    private readonly exchangesService: ExchangesService,
    private readonly ordersTemplatesService: OrdersTemplatesService,
  ) {}

  @Post()
  async createTemplate(@Body() body: CreateTemplateBody) {
    const exchange = await this.exchangesService.getOne(body.exchangeId);

    return this.ordersTemplatesService.createTemplate(
      exchange,
      body.template,
      body.type,
    );
  }
}
