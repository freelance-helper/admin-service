import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { OrdersTemplateEntity } from './orders-template.entity';
import { OrdersTemplatesService } from './orders-templates.service';
import { OrdersTemplatesController } from './orders-templates.controller';
import { ExchangesService, ExchangesModule } from '@/modules/exchanges';

@Module({
  imports: [TypeOrmModule.forFeature([OrdersTemplateEntity]), ExchangesModule],
  controllers: [OrdersTemplatesController],
  providers: [OrdersTemplatesService, ExchangesService],
})
export class OrdersTemplatesModule {}
