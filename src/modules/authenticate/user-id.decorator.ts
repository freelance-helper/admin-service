import { createParamDecorator } from '@nestjs/common';

export const UserID = createParamDecorator(
  (data: string, req): number => {
    return Number(req.headers['x-user-id']);
  },
);
