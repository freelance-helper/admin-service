import { createParamDecorator } from '@nestjs/common';
import { getTokenFromHeader } from '@/libs/jwt';

export const Token = createParamDecorator(
  (data: string, req): string | null => {
    return req.headers.authorization
      ? getTokenFromHeader(req.headers.authorization)
      : null;
  },
);
