import { Module } from '@nestjs/common';

import { AuthenticateController } from './authenticate.controller';
import { UsersModule } from '@/modules/users/users.module';
import { UsersService } from '@/modules/users/users.service';

@Module({
  imports: [UsersModule],
  controllers: [AuthenticateController],
  providers: [UsersService],
})
export class AuthenticateModule {}
