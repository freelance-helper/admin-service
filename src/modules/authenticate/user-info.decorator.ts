import { createParamDecorator } from '@nestjs/common';

import { decode, getTokenFromHeader } from '@/libs/jwt';
import { JWTPayload } from '@/libs/jwt.d';

export const UserInfo = createParamDecorator(
  (data: string, req): JWTPayload => {
    const token = getTokenFromHeader(req.headers.authorization);

    return req.headers.authorization ? decode(token) : {};
  },
);
