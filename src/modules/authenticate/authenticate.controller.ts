import { Controller, Post, Get, Body, Param, Res } from '@nestjs/common';
import { ConfigService } from 'nestjs-config';
import { MailerService } from '@nest-modules/mailer';
import * as qrcode from 'qrcode';

import { UserInfo } from './user-info.decorator';
import { Token } from './token.decorator';
import { JWTPayload } from '@/libs/jwt.d';
import { createToken, isTokenValid, decode } from '@/libs/jwt';

import { UsersService } from '@/modules/users';
import { LoggerService } from '@/libs/logger';

interface IAuthenticateInput {
  email: string;
  password: string;
}

@Controller('authenticate')
export class AuthenticateController {
  constructor(
    private readonly config: ConfigService,
    private readonly usersService: UsersService,
    private readonly mailer: MailerService,
    private readonly logger: LoggerService,
  ) {}

  @Post('registration')
  async registration(
    @Body() body: IAuthenticateInput,
    @Res() res,
  ): Promise<void> {
    if (!body.email || !body.password) {
      res.status(400).send({ error: true, message: 'Invalid credentials' });
      return;
    }

    const user = await this.usersService.getByEmail(body.email);

    if (user) {
      res.status(409).send({ error: true, message: 'User already exist' });
      return;
    }

    const createdUser = await this.usersService.createUser(
      body.email,
      body.password,
    );
    const credentials = await this.usersService.getUserCredentials(
      createdUser.id,
    );

    const emailPayload = {
      to: body.email,
      subject: 'Добро пожаловать в Notificator!',
      template: 'activate-account',
      context: {
        link: `${this.config.get('app.frontUrl')}/authenticate/activate?id=${
          credentials.activationCode.id
        }`,
        front: `${this.config.get('app.frontUrl')}/`,
      },
    };

    try {
      await this.mailer.sendMail(emailPayload);

      res.status(201).send({ message: 'Вы успешно зарегестрированы' });
    } catch (e) {
      await this.usersService.deleteUser(createdUser.id);
      this.logger.error('Send email error', e, AuthenticateController.name);
      res
        .status(500)
        .send({ message: 'Извините, произошла ошибка, попробуйте позже' });
    }
    return;
  }

  @Post('login')
  async login(@Body() body: IAuthenticateInput, @Res() res) {
    if (!body.email || !body.password) {
      res.status(403).send({
        error: true,
        message: 'Invalid credentials',
      });
      return;
    }

    const user = await this.usersService.getByEmail(body.email);

    if (!user) {
      res.status(404).send({
        error: true,
        message: 'User not found',
      });
      return;
    }

    const passwordIsCorrect = await this.usersService.passwordIsValid(
      user.id,
      body.password,
    );

    if (!passwordIsCorrect) {
      return res.status(403).send({
        error: true,
        message: 'Incorrect email or password',
      });
    }

    const payload = {
      id: user.id,
    };

    return res.status(200).send({
      user,
      expiresIn: this.getTokenExpires(),
      accessToken: this.createAccessToken(payload),
      refreshToken: this.createRefreshToken(payload),
    });
  }

  @Get('refresh-token/:refreshToken')
  refreshToken(@Param('refreshToken') refreshToken, @Res() res) {
    if (!isTokenValid(refreshToken, this.config.get('app.secret'))) {
      res.status(401).send({
        error: true,
        message: 'You need login again',
      });
    }

    const payload = decode(refreshToken);

    res.status(201).send({
      accessToken: this.createAccessToken(payload),
      refreshToken: this.createRefreshToken(payload),
      expiresIn: this.getTokenExpires(),
    });
  }

  @Get('telegram')
  async getQR(@UserInfo() user, @Token() token) {
    const payload = JSON.stringify({ token });

    const qr = await qrcode.toDataURL(payload);

    return { qrcode: qr };
  }

  @Post('telegram')
  async authenticateTelegram(
    @Body('tid') tid,
    @UserInfo() payload: JWTPayload,
    @Res() res,
  ) {
    if (!payload.id) {
      res.status(404).send({ message: 'User not found' });
    }

    const user = await this.usersService.addTelegramAccount(payload.id, tid);

    res.status(201).send({
      userId: user.id,
    });
  }

  @Post('create-password-recovery')
  async getPasswordRecoveryCode(@Body('email') email: string) {
    return '';
  }

  @Post('')
  async passwordRecovery(
    @Body('email') email: string,
    @Body('code') code: string,
  ) {
    return '';
  }

  getTokenExpires() {
    return (
      Math.trunc(Date.now() / 1000) + this.config.get('app.accessTokenExpires')
    );
  }

  createAccessToken(payload: JWTPayload) {
    return createToken(
      this.config.get('app.accessTokenExpires'),
      payload,
      this.config.get('app.secret'),
    );
  }

  createRefreshToken(payload: JWTPayload) {
    return createToken(
      this.config.get('app.refreshTokenExpires'),
      payload,
      this.config.get('app.secret'),
    );
  }
}
