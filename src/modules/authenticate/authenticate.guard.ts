import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Observable } from 'rxjs';
import { ConfigService } from 'nestjs-config';

import { decodeToken, getTokenFromHeader } from '@/libs/jwt';
import { LoggerService } from '@/libs/logger';
import { JWTPayload } from '@/libs/jwt.d';

@Injectable()
export class AuthenticateGuard implements CanActivate {
  constructor(
    private readonly config: ConfigService,
    private readonly logger: LoggerService,
  ) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();

    return this.validateRequest(request);
  }

  async validateRequest(request) {
    const header = request.headers.authorization;
    const userId = request.headers['x-user-id'];

    if (!header) {
      this.logger.log(
        'Auth false, without Authorization headers',
        AuthenticateGuard.name,
      );
      return false;
    }

    if (!userId) {
      this.logger.log('Auth false, without x-user-id', AuthenticateGuard.name);
      return false;
    }

    const token = getTokenFromHeader(header);
    const secret = this.config.get('app.secret');

    try {
      const data: JWTPayload = await decodeToken(token, secret);

      return Number(userId) === data.id;
    } catch (e) {
      this.logger.log('Auth false, token invalid', AuthenticateGuard.name);
      return false;
    }
  }
}
