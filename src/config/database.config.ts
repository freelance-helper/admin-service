import { SnakeNamingStrategy } from '../libs/typeorm-snakecase-naming';

export default {
  type: 'mysql',
  host: process.env.TYPEORM_HOST,
  port: process.env.TYPEORM_PORT,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  synchronize: process.env.TYPEORM_SYNCHRONIZE,
  entities: [__dirname + '/../**/*.entity{.js,.ts}'],
  namingStrategy: new SnakeNamingStrategy(),
};
