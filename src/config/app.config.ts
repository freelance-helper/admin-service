export default {
  port: process.env.PORT || 3000,
  secret: process.env.SECRET || 'secret',
  botServiceHost: process.env.BOT_SERVICE_HOST,
  loggerLevel: process.env.LOGGER_LEVEL || 'INFO', // INFO,WARNING,ERROR
  accessTokenExpires: parseInt(process.env.ACCESS_TOKEN_EXPIRES || '3600', 10),
  refreshTokenExpires: parseInt(
    process.env.REFRESH_TOKEN_EXPIRES || '36000',
    10,
  ),
  firebaseMessagesServerKey: process.env.FIREBASE_MESSAGES_SERVER_KEY,
  frontUrl: process.env.FRONT_URL,
};
