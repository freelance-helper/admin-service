export default {
  email: process.env.MAILER_EMAIL,
  password: process.env.MAILER_PASSWORD,
  domain: process.env.MAILER_DOMAIN,
  clientId: process.env.MAILER_CLIENT_ID,
  clientSecret: process.env.MAILER_CLIENT_SECRET,
  refreshToken: process.env.MAILER_REFRESH_TOKEN,
  accessToken: process.env.MAILER_ACCESS_TOKEN,
};
