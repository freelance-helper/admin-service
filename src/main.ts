import { NestFactory } from '@nestjs/core';
import { ConfigService } from 'nestjs-config';

import { AppModule } from './modules/app';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService: ConfigService = app.get(ConfigService);

  app.enableCors({
    origin: '*',
  });
  const port = configService.get('app.port') || 3000;
  await app.listen(port);

  console.log(`Starting in ${port} port`);
}
bootstrap();
