import { sign, verify, decode } from 'jsonwebtoken';
import { JWTPayload } from './jwt.d';

export function createToken(
  expiresIn: string,
  payload: JWTPayload,
  secret: string,
) {
  return sign(
    {
      ...payload,
    },
    secret,
    {
      expiresIn: parseInt(expiresIn, 10),
    },
  );
}

export function isTokenValid(token: string, secret: string) {
  try {
    verify(token, secret);

    return true;
  } catch (e) {
    return false;
  }
}

export function decodeToken(
  token: string,
  secret: string,
): Promise<JWTPayload> {
  return new Promise((resolve, reject) =>
    verify(token, secret, (err, decoded) => {
      if (err) {
        reject('Token expired');
      } else {
        resolve(decoded);
      }
    }),
  );
}

export function decode(token) {
  const { iat, exp, ...payload } = decode(token) as any;

  return payload;
}

export function getTokenFromHeader(header: string): string | null {
  return header ? header.split(' ')[1] : null;
}
