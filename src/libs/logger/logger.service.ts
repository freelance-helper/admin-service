import { Injectable, LoggerService as ILoggerService } from '@nestjs/common';
import { ConfigService } from 'nestjs-config';

@Injectable()
export class LoggerService implements ILoggerService {
  levels = {
    info: 'INFO',
    warn: 'WARN',
    error: 'ERROR',
    off: 'off',
  };

  constructor(private readonly config: ConfigService) {}

  log(message: any, context?: string): any {
    if (this.isLevelsInclude(this.levels.info)) {
      this.print(message, this.levels.info, context);
    }
  }
  warn(message: any, context?: string): any {
    if (
      this.isLevelsInclude(this.levels.warn) ||
      this.isLevelsInclude(this.levels.info)
    ) {
      this.print(message, this.levels.warn, context);
    }
  }
  error(message: any, trace?: string, context?: string): any {
    if (
      this.isLevelsInclude(this.levels.warn) ||
      this.isLevelsInclude(this.levels.info) ||
      this.isLevelsInclude(this.levels.error)
    ) {
      this.print(`${message}\n${trace}`, this.levels.error, context);
    }
  }

  print(message: string, level: string, context?: string) {
    if (this.isLevelsInclude(this.levels.off)) {
      return;
    }

    const formatMessage = context
      ? `${new Date().toISOString()}, ${level}, ${context}: ${message}`
      : `${new Date().toISOString()}, ${level}: ${message}`;

    console.log(formatMessage);
  }

  isLevelsInclude(level) {
    const levels = this.config.get('app.loggerLevel');

    return levels.split(',').includes(level);
  }
}
