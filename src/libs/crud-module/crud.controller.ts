import { Post, Get, Delete, Param, Body } from '@nestjs/common';
import { ICrudService } from './crud';

export abstract class CRUDController<IInputCreateData, IEntity> {
  protected constructor(
    protected readonly service: ICrudService<IInputCreateData, IEntity>,
  ) {}

  @Post()
  public async createEntity(
    @Body('data') data: IInputCreateData,
  ): Promise<IEntity> {
    return this.service.createEntity(data);
  }

  @Get()
  public async getAll(): Promise<IEntity[]> {
    return this.service.getAllEntities();
  }

  @Delete(':id')
  public async delete(@Param('id') id: number): Promise<void> {
    await this.service.deleteEntity(id);

    return;
  }
}
