import { Repository } from 'typeorm';

import { ICrudService } from './crud';

export abstract class CRUDService<IInputCreateData, IEntity>
  implements ICrudService<IInputCreateData, IEntity> {
  protected constructor(private readonly repository: Repository<IEntity>) {}

  async createEntity(entity: IInputCreateData): Promise<IEntity> {
    return this.repository.save(entity);
  }

  async getAllEntities(): Promise<IEntity[]> {
    return this.repository.find();
  }

  async deleteEntity(id: number): Promise<void> {
    const entity = await this.repository.findOne(id);

    await this.repository.remove(entity);

    return;
  }
}
