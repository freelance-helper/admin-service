export interface ICrudService<IInputCreateData, IEntity> {
  createEntity(data: IInputCreateData): Promise<IEntity>;
  getAllEntities(): Promise<IEntity[]>;
  deleteEntity(id: number): Promise<void>;
}
