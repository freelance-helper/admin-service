import { Module, HttpService, HttpModule } from '@nestjs/common';
import { FirebaseMessagesService } from './firebase-messages.service';

@Module({
  imports: [HttpModule],
  providers: [FirebaseMessagesService, HttpService],
  exports: [FirebaseMessagesService],
})
export class FirebaseMessagesModule {}
