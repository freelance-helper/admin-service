import { Injectable, HttpService } from '@nestjs/common';
import { ConfigService } from 'nestjs-config';

@Injectable()
export class FirebaseMessagesService {
  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  async send(body: any, token: string) {
    const url = 'https://fcm.googleapis.com/fcm/send';
    const headers = {
      'Authorization': `key=${this.config.get('app.firebaseMessagesServerKey')}`,
      'Content-Type': 'application/json',
    };
    const config = { headers };
    const data = {
      to: token,
      notification: body,
    };

    return this.http.post(url, data, config).toPromise();
  }
}
